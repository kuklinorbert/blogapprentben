import 'package:blogapprentben/util/size_config.dart';
import 'package:flutter/material.dart';

extension IntX on int {
  double get psw => toDouble().psw;
  double get psh => toDouble().psh;
  double get pfs => toDouble().pfs;
}

extension DoubleX on double {
  double get psw => SizeConfig.getProportionateScreenWidth(this);
  double get psh => SizeConfig.getProportionateScreenHeight(this);
  double get pfs => SizeConfig.getProportionateScreenHeight(this);
}

///This method is used to set padding responsively
EdgeInsetsGeometry getPadding({
  double? all,
  double? left,
  double? top,
  double? right,
  double? bottom,
}) {
  return getMarginOrPadding(
    all: all,
    left: left,
    top: top,
    right: right,
    bottom: bottom,
  );
}

EdgeInsetsGeometry getMargin({
  double? all,
  double? left,
  double? top,
  double? right,
  double? bottom,
}) {
  return getMarginOrPadding(
    all: all,
    left: left,
    top: top,
    right: right,
    bottom: bottom,
  );
}

EdgeInsetsGeometry getMarginOrPadding({
  double? all,
  double? left,
  double? top,
  double? right,
  double? bottom,
}) {
  if (all != null) {
    left = all;
    top = all;
    right = all;
    bottom = all;
  }
  return EdgeInsets.only(
    left: left != null ? left.psw : 0,
    top: top != null ? top.psh : 0,
    right: right != null ? right.psw : 0,
    bottom: bottom != null ? bottom.psh : 0,
  );
}
