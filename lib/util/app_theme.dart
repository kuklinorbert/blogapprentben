import 'package:blogapprentben/util/app_color.dart';
import 'package:blogapprentben/util/extension.dart';
import 'package:flutter/material.dart';

class AppTheme {
  static final horizontalLargePadding = 30.psw;
  static final horizontalPadding = 20.psw;
  static final horizontalSmallPadding = 10.psw;
  static final verticalLargePadding = 30.psh;
  static final verticalPadding = 20.psh;
  static final verticalSmallPadding = 10.psh;

  static final normalTextSize = 17.psh;
  static final separatorLineHeight = 0.5.psh;
  static final boxRounding = 4.psh;

  static const bottomSheetShape = RoundedRectangleBorder(
    borderRadius: BorderRadius.only(
      topLeft: Radius.circular(13.0),
      topRight: Radius.circular(13.0),
    ),
  );

  static final orderNumberTextStyle = TextStyle(
      fontWeight: FontWeight.w700,
      fontStyle: FontStyle.normal,
      fontSize: 13.0.pfs);

  static final normal35TextStyle = TextStyle(fontSize: 35.pfs);

  static final bolder35TextStyle =
      TextStyle(fontSize: 35.pfs, fontWeight: FontWeight.w500);

  static final bolder25TextStyle =
      TextStyle(fontSize: 25.pfs, fontWeight: FontWeight.w500);

  static final normal25TextStyle =
      TextStyle(fontSize: 25.pfs, fontWeight: FontWeight.w500);

  static final bolder20TextStyle =
      TextStyle(fontSize: 20.pfs, fontWeight: FontWeight.w500);

  static final normal20TextStyle = TextStyle(
    fontSize: 20.pfs,
  );

  static final light18TextStyle = TextStyle(
      fontSize: 18.pfs,
      fontWeight: FontWeight.w300,
      color: AppColor.lightTextColor);

  static final normal18TextStyle = TextStyle(
    fontSize: 18.pfs,
  );

  static final normal15TextStyle = TextStyle(fontSize: 15.pfs);

  static final bold15TextStyle =
      TextStyle(fontSize: 15.pfs, fontWeight: FontWeight.w600);

  static final normal12TextStyle = TextStyle(
    fontSize: 12.psh,
  );
}
