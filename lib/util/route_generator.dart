import 'package:blogapprentben/features/presentation/screen/login/login_screen.dart';
import 'package:blogapprentben/features/presentation/screen/main/main_screen.dart';
import 'package:blogapprentben/util/app_key.dart';
import 'package:flutter/material.dart';

abstract class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case AppKey.routeMain:
        return MaterialPageRoute(
          builder: (_) => MainScreen(),
          settings: RouteSettings(name: AppKey.routeMain),
        );

      case AppKey.routeLogin:
      default:
        return MaterialPageRoute(
          builder: (_) => LoginScreen(),
          settings: RouteSettings(name: AppKey.routeLogin),
        );
    }
  }
}
