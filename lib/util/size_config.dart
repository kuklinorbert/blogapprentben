import 'package:flutter/material.dart';

class SizeConfig {
  static late double _screenWidth;
  static late double _screenHeight;

  static void init(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    _screenWidth = screenSize.width;
    _screenHeight = screenSize.height;
  }

  static double getProportionateScreenWidth(double inputWidth) {
    return (inputWidth / 375.0) * _screenWidth;
  }

  static double getProportionateScreenHeight(double inputHeight) {
    return (inputHeight / 812.0) * _screenHeight;
  }
}
