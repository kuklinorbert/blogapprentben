import 'package:flutter/material.dart';

class AppKey {
  static final navigatorKey = GlobalKey<NavigatorState>();

  static const routeSplash = '/';
  static const routeLogin = '/login';
  static const routeMain = '/main';

  static const hiveBoxUserData = 'userData';
  static const hiveKeyUserId = 'userId';
  static const hiveKeyBlogData = 'blogList';
}
