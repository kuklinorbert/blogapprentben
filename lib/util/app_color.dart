import 'package:flutter/material.dart';

class AppColor {
  static const green = Color(0xFF08A045);
  static const red = Color(0xFFe22d2d);

  static const lighterGrey = Color(0xFFe7e7e7);
  static const lightGrey = Color(0xFFbfbfbf);
  static const grey = Color(0xFFa2a2a2);
  static const backgroundColor = Color(0xFFe4feee);
  static const textFieldFillColor = Color(0xFFf7fffa);
  static const borderGreen = Color(0xFF2E7D32);
  static const overlayDarkGreen = Color(0xFF1B5E20);
  static const lightTextColor = Colors.black54;

  static MaterialColor createMaterialColor(Color color) {
    var strengths = <double>[.05];
    final swatch = <int, Color>{};
    final int r = color.red;
    final int g = color.green;
    final int b = color.blue;

    for (int i = 1; i < 10; i++) {
      strengths.add(0.1 * i);
    }
    for (var strength in strengths) {
      final double ds = 0.5 - strength;
      swatch[(strength * 1000).round()] = Color.fromRGBO(
        r + ((ds < 0 ? r : (255 - r)) * ds).round(),
        g + ((ds < 0 ? g : (255 - g)) * ds).round(),
        b + ((ds < 0 ? b : (255 - b)) * ds).round(),
        1,
      );
    }
    return MaterialColor(color.value, swatch);
  }
}
