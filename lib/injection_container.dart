import 'package:blogapprentben/features/data/datasources/auth_data_source.dart';
import 'package:blogapprentben/features/data/datasources/blog_data_source.dart';
import 'package:blogapprentben/features/data/datasources/user_data_source.dart';
import 'package:blogapprentben/features/data/repositories/auth_repository.dart';
import 'package:blogapprentben/features/data/repositories/blog_repository.dart';
import 'package:blogapprentben/features/data/repositories/user_repository.dart';
import 'package:blogapprentben/features/domain/usecases/add_blog_use_case.dart';
import 'package:blogapprentben/features/domain/usecases/delete_blog_use_case.dart';
import 'package:blogapprentben/features/domain/usecases/get_blog_list_use_case.dart';
import 'package:blogapprentben/features/domain/usecases/get_user_data_use_case.dart';
import 'package:blogapprentben/features/domain/usecases/get_user_list_use_case.dart';
import 'package:blogapprentben/features/domain/usecases/login_use_case.dart';
import 'package:blogapprentben/features/domain/usecases/logout_use_case.dart';
import 'package:blogapprentben/features/domain/usecases/update_blog_use_case.dart';
import 'package:blogapprentben/features/presentation/bloc/blog/add_blog/add_blog_bloc.dart';
import 'package:blogapprentben/features/presentation/bloc/blog/blog_list/blog_list_bloc.dart';
import 'package:blogapprentben/features/presentation/bloc/blog/delete_blog/delete_blog_bloc.dart';
import 'package:blogapprentben/features/presentation/bloc/blog/update_blog/update_blog_bloc.dart';
import 'package:blogapprentben/features/presentation/bloc/login/login_bloc.dart';
import 'package:blogapprentben/features/presentation/bloc/user_data/user_data_bloc.dart';
import 'package:blogapprentben/features/presentation/bloc/user_list/user_list_bloc.dart';
import 'package:get_it/get_it.dart';

final sl = GetIt.instance;

Future<void> init() async {
  //BLOC
  sl.registerLazySingleton(() => LoginBloc(login: sl(), logout: sl()));
  sl.registerLazySingleton(() => UserDataBloc(getUserData: sl()));
  sl.registerLazySingleton(() => BlogListBloc(getBlogList: sl()));
  sl.registerLazySingleton(() => AddBlogBloc(addBlog: sl()));
  sl.registerLazySingleton(() => DeleteBlogBloc(deleteBlog: sl()));
  sl.registerLazySingleton(() => UpdateBlogBloc(updateBlog: sl()));
  sl.registerLazySingleton(() => UserListBloc(getUserList: sl()));

  //USE CASE
  sl.registerLazySingleton(() => LoginUseCase(sl()));
  sl.registerLazySingleton(() => LogoutUseCase(sl()));
  sl.registerLazySingleton(() => GetUserDataUseCase(sl()));
  sl.registerLazySingleton(() => GetBlogListUseCase(sl()));
  sl.registerLazySingleton(() => AddBlogUseCase(sl()));
  sl.registerLazySingleton(() => DeleteBlogUseCase(sl()));
  sl.registerLazySingleton(() => UpdateBlogUseCase(sl()));
  sl.registerLazySingleton(() => GetUserListUseCase(sl()));

  //REPOSITORY
  sl.registerLazySingleton<UserRepository>(
      () => UserRepositoryImpl(userDataSource: sl(), blogDataSource: sl()));
  sl.registerLazySingleton<AuthRepository>(
      () => AuthRepositoryImpl(authDataSource: sl(), userDataSource: sl()));
  sl.registerLazySingleton<BlogRepository>(
      () => BlogRepositoryImpl(blogDataSource: sl(), userDataSource: sl()));

  //DATA SOURCE
  sl.registerLazySingleton<AuthDataSource>(() => AuthDataSourceImpl());
  sl.registerLazySingleton<UserDataSource>(() => UserDataSourceImpl());
  sl.registerLazySingleton<BlogDataSource>(() => BlogDataSourceImpl());
}
