import 'package:bloc/bloc.dart';
import 'package:blogapprentben/features/domain/entities/blog_data.dart';
import 'package:blogapprentben/features/domain/usecases/update_blog_use_case.dart';
import 'package:equatable/equatable.dart';

part 'update_blog_event.dart';
part 'update_blog_state.dart';

class UpdateBlogBloc extends Bloc<UpdateBlogEvent, UpdateBlogState> {
  final UpdateBlogUseCase updateBlog;

  UpdateBlogBloc({required this.updateBlog}) : super(UpdateBlogInitial()) {
    on<UpdateBlogEvent>((event, emit) async {
      if (event is UpdateBlogItemEvent) {
        emit(UpdateBlogLoading());
        final result =
            await updateBlog.call(UpdateBlogParams(blogData: event.blogData));
        result.fold(
          (error) {
            print("update blog error");
            emit(UpdateBlogError());
          },
          (response) =>
              response ? emit(UpdateBlogSuccess()) : emit(UpdateBlogError()),
        );
      }
    });
  }
}
