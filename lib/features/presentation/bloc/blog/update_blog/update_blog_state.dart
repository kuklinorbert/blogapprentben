part of 'update_blog_bloc.dart';

class UpdateBlogState extends Equatable {
  const UpdateBlogState();

  @override
  List<Object> get props => [];
}

class UpdateBlogInitial extends UpdateBlogState {}

class UpdateBlogLoading extends UpdateBlogState {}

class UpdateBlogSuccess extends UpdateBlogState {}

class UpdateBlogError extends UpdateBlogState {}
