part of 'update_blog_bloc.dart';

class UpdateBlogEvent extends Equatable {
  const UpdateBlogEvent();

  @override
  List<Object> get props => [];
}

class UpdateBlogItemEvent extends UpdateBlogEvent {
  const UpdateBlogItemEvent({required this.blogData});

  final BlogData blogData;

  @override
  List<Object> get props => [
        blogData,
      ];
}
