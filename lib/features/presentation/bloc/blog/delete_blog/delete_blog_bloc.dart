import 'package:bloc/bloc.dart';
import 'package:blogapprentben/features/domain/usecases/delete_blog_use_case.dart';
import 'package:equatable/equatable.dart';

part 'delete_blog_event.dart';
part 'delete_blog_state.dart';

class DeleteBlogBloc extends Bloc<DeleteBlogEvent, DeleteBlogState> {
  final DeleteBlogUseCase deleteBlog;
  DeleteBlogBloc({required this.deleteBlog}) : super(DeleteBlogInitial()) {
    on<DeleteBlogEvent>((event, emit) async {
      if (event is DeleteBlogByIdEvent) {
        emit(DeleteBlogLoading());
        final result =
            await deleteBlog.call(DeleteBlogParams(blogId: event.blogId));
        result.fold(
          (error) {
            print("delete blog  error");
            emit(DeleteBlogError());
          },
          (response) =>
              response ? emit(DeleteBlogSuccess()) : emit(DeleteBlogError()),
        );
      }
    });
  }
}
