part of 'delete_blog_bloc.dart';

class DeleteBlogEvent extends Equatable {
  const DeleteBlogEvent();

  @override
  List<Object> get props => [];
}

class DeleteBlogByIdEvent extends DeleteBlogEvent {
  const DeleteBlogByIdEvent({required this.blogId});

  final int blogId;

  @override
  List<Object> get props => [
        blogId,
      ];
}
