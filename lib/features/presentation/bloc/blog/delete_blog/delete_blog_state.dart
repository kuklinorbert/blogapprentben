part of 'delete_blog_bloc.dart';

class DeleteBlogState extends Equatable {
  const DeleteBlogState();

  @override
  List<Object> get props => [];
}

class DeleteBlogInitial extends DeleteBlogState {}

class DeleteBlogLoading extends DeleteBlogState {}

class DeleteBlogSuccess extends DeleteBlogState {}

class DeleteBlogError extends DeleteBlogState {}
