part of 'add_blog_bloc.dart';

class AddBlogState extends Equatable {
  const AddBlogState();

  @override
  List<Object> get props => [];
}

class AddBlogInitial extends AddBlogState {}

class AddBlogLoading extends AddBlogState {}

class AddBlogSuccess extends AddBlogState {}

class AddBlogError extends AddBlogState {}
