part of 'add_blog_bloc.dart';

class AddBlogEvent extends Equatable {
  const AddBlogEvent();

  @override
  List<Object> get props => [];
}

class AddNewBlogEvent extends AddBlogEvent {
  const AddNewBlogEvent({required this.title, required this.content});

  final String title;
  final String content;

  @override
  List<Object> get props => [title, content];
}
