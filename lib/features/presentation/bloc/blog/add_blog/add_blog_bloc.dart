import 'package:bloc/bloc.dart';
import 'package:blogapprentben/features/domain/usecases/add_blog_use_case.dart';
import 'package:equatable/equatable.dart';

part 'add_blog_event.dart';
part 'add_blog_state.dart';

class AddBlogBloc extends Bloc<AddBlogEvent, AddBlogState> {
  final AddBlogUseCase addBlog;
  AddBlogBloc({required this.addBlog}) : super(AddBlogInitial()) {
    on<AddBlogEvent>((event, emit) async {
      if (event is AddNewBlogEvent) {
        emit(AddBlogLoading());
        final result = await addBlog
            .call(AddBlogParams(title: event.title, content: event.content));
        result.fold(
          (error) {
            print("add blog error");
            emit(AddBlogError());
          },
          (response) =>
              response ? emit(AddBlogSuccess()) : emit(AddBlogError()),
        );
      }
    });
  }
}
