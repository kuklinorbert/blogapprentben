part of 'blog_list_bloc.dart';

class BlogListEvent extends Equatable {
  const BlogListEvent();

  @override
  List<Object> get props => [];
}

class LoadBlogListEvent extends BlogListEvent {}
