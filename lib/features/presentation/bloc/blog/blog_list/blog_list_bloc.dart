import 'package:bloc/bloc.dart';
import 'package:blogapprentben/core/usecases.dart';
import 'package:blogapprentben/features/domain/entities/blog_data.dart';
import 'package:blogapprentben/features/domain/usecases/get_blog_list_use_case.dart';
import 'package:equatable/equatable.dart';

part 'blog_list_event.dart';
part 'blog_list_state.dart';

class BlogListBloc extends Bloc<BlogListEvent, BlogListState> {
  GetBlogListUseCase getBlogList;
  BlogListBloc({required this.getBlogList}) : super(BlogListInitial()) {
    on<BlogListEvent>((event, emit) async {
      if (event is LoadBlogListEvent) {
        emit(BlogListLoading());
        final result = await getBlogList.call(NoParams());
        result.fold(
          (error) {
            print("get blog list error");
            emit(BlogListError());
          },
          (response) => emit(BlogListLoaded(blogList: response)),
        );
      }
    });
  }
}
