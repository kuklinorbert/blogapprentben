part of 'blog_list_bloc.dart';

class BlogListState extends Equatable {
  const BlogListState();

  @override
  List<Object> get props => [];
}

class BlogListInitial extends BlogListState {}

class BlogListLoading extends BlogListState {}

class BlogListLoaded extends BlogListState {
  final List<BlogData> blogList;

  const BlogListLoaded({required this.blogList});

  @override
  List<Object> get props => [blogList];
}

class BlogListError extends BlogListState {}
