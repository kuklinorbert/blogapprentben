part of 'login_bloc.dart';

class LoginEvent extends Equatable {
  const LoginEvent();

  @override
  List<Object> get props => [];
}

class CheckLoginEvent extends LoginEvent {}

class LoginUserEvent extends LoginEvent {
  const LoginUserEvent({required this.email, required this.password});

  final String email;
  final String password;

  @override
  List<Object> get props => [email, password];
}

class ResetLoginEvent extends LoginEvent {}

class LogoutEvent extends LoginEvent {}
