import 'package:bloc/bloc.dart';
import 'package:blogapprentben/core/usecases.dart';
import 'package:blogapprentben/features/domain/usecases/login_use_case.dart';
import 'package:blogapprentben/features/domain/usecases/logout_use_case.dart';
import 'package:equatable/equatable.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final LoginUseCase login;
  final LogoutUseCase logout;

  LoginBloc({required this.login, required this.logout})
      : super(LoginInitial()) {
    on<LoginEvent>((event, emit) async {
      if (event is LoginUserEvent) {
        emit(LoginLoadingState());
        final response = await login
            .call(LoginParams(email: event.email, password: event.password));
        response.fold(
            (error) => emit(
                  LoginErrorState(),
                ),
            (response) =>
                response ? emit(LoginSuccessState()) : emit(LoginErrorState()));
      }

      if (event is ResetLoginEvent) {
        emit(LoginInitial());
      }
      if (event is LogoutEvent) {
        final response = await logout.call(NoParams());
        response.fold((error) => print("logout error"), (response) {
          emit(LogoutSuccessState());
        });
      }
    });
  }
}
