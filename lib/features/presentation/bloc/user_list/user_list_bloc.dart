import 'package:bloc/bloc.dart';
import 'package:blogapprentben/core/usecases.dart';
import 'package:blogapprentben/features/domain/entities/user_data.dart';
import 'package:blogapprentben/features/domain/usecases/get_user_list_use_case.dart';
import 'package:equatable/equatable.dart';

part 'user_list_event.dart';
part 'user_list_state.dart';

class UserListBloc extends Bloc<UserListEvent, UserListState> {
  final GetUserListUseCase getUserList;

  UserListBloc({required this.getUserList}) : super(UserListInitial()) {
    on<UserListEvent>((event, emit) async {
      if (event is GetUserListEvent) {
        emit(UserListLoading());
        final result = await getUserList.call(NoParams());
        result.fold(
          (error) {
            print("get user list error");
            emit(UserListError());
          },
          (response) => emit(UserListLoaded(userList: response)),
        );
      }
    });
  }
}
