part of 'user_list_bloc.dart';

class UserListEvent extends Equatable {
  const UserListEvent();

  @override
  List<Object> get props => [];
}

class GetUserListEvent extends UserListEvent {}
