part of 'user_data_bloc.dart';

class UserDataEvent extends Equatable {
  const UserDataEvent();

  @override
  List<Object> get props => [];
}

class GetUserDataEvent extends UserDataEvent {}

class ResetUserDataEvent extends UserDataEvent {}
