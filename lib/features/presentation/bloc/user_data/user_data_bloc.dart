import 'package:bloc/bloc.dart';
import 'package:blogapprentben/core/usecases.dart';
import 'package:blogapprentben/features/domain/entities/user_data.dart';
import 'package:blogapprentben/features/domain/usecases/get_user_data_use_case.dart';
import 'package:equatable/equatable.dart';

part 'user_data_event.dart';
part 'user_data_state.dart';

class UserDataBloc extends Bloc<UserDataEvent, UserDataState> {
  final GetUserDataUseCase getUserData;

  UserDataBloc({required this.getUserData}) : super(UserDataInitial()) {
    on<UserDataEvent>((event, emit) async {
      if (event is GetUserDataEvent) {
        emit(UserDataLoading());
        final result = await getUserData.call(NoParams());
        result.fold(
          (error) {
            print("get user data error");
            emit(UserDataError());
          },
          (response) => emit(UserDataLoaded(userData: response)),
        );
      }
      if (event is ResetUserDataEvent) {
        emit(UserDataInitial());
      }
    });
  }
}
