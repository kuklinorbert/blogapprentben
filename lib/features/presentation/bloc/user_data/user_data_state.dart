part of 'user_data_bloc.dart';

class UserDataState extends Equatable {
  const UserDataState();

  @override
  List<Object> get props => [];
}

class UserDataInitial extends UserDataState {}

class UserDataLoading extends UserDataState {}

class UserDataLoaded extends UserDataState {
  final UserData userData;

  const UserDataLoaded({required this.userData});

  @override
  List<Object> get props => [userData];
}

class UserDataError extends UserDataState {}
