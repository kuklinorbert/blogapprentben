import 'package:blogapprentben/features/domain/entities/user_data.dart';
import 'package:blogapprentben/features/presentation/bloc/login/login_bloc.dart';
import 'package:blogapprentben/features/presentation/bloc/user_data/user_data_bloc.dart';
import 'package:blogapprentben/features/presentation/widgets/blog_list_view.dart';
import 'package:blogapprentben/features/presentation/widgets/custom_text_button.dart';
import 'package:blogapprentben/injection_container.dart';
import 'package:blogapprentben/util/app_theme.dart';
import 'package:blogapprentben/util/extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage>
    with AutomaticKeepAliveClientMixin {
  LoginBloc authBloc = sl<LoginBloc>();
  UserDataBloc userDataBloc = sl<UserDataBloc>();

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder<UserDataBloc, UserDataState>(
      bloc: userDataBloc,
      builder: (context, state) {
        if (state is UserDataLoaded) {
          final UserData userData = state.userData;
          return SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(height: 40.psh),
                Padding(
                  padding: getPadding(left: 30.psw, right: 30.psw),
                  child: Text(
                    userData.name,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: AppTheme.normal35TextStyle,
                  ),
                ),
                SizedBox(height: 10.psh),
                Text(
                  "Posztok száma: ${userData.userBlogs.length}",
                  style: AppTheme.light18TextStyle,
                ),
                SizedBox(height: 25.psh),
                CustomTextButton(
                  text: "Kijelentkezés",
                  onTap: () {
                    authBloc.add(LogoutEvent());
                  },
                ),
                BlogListView(
                  blogList: userData.userBlogs,
                  shrinkWrap: true,
                  userData: userData,
                )
              ],
            ),
          );
        }

        return Center(
            child: SizedBox(
                width: 50.psh,
                height: 50.psh,
                child: const CircularProgressIndicator(
                  strokeWidth: 4,
                )));
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}
