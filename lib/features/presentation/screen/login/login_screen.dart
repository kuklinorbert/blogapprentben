import 'package:blogapprentben/features/presentation/bloc/login/login_bloc.dart';
import 'package:blogapprentben/features/presentation/widgets/custom_text_input_field.dart';
import 'package:blogapprentben/features/presentation/widgets/custom_text_button.dart';
import 'package:blogapprentben/injection_container.dart';
import 'package:blogapprentben/util/app_color.dart';
import 'package:blogapprentben/util/app_key.dart';
import 'package:blogapprentben/util/app_theme.dart';
import 'package:blogapprentben/util/extension.dart';
import 'package:blogapprentben/util/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  LoginBloc loginBloc = sl<LoginBloc>();

  final emailEditingController = TextEditingController();
  final passwordEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    return BlocListener<LoginBloc, LoginState>(
      bloc: loginBloc,
      listener: (context, state) {
        if (state is LoginSuccessState) {
          Navigator.pushReplacementNamed(context, AppKey.routeMain);
        }
        if (state is LoginErrorState) {
          ScaffoldMessenger.of(context!).showSnackBar(SnackBar(
            content: Text(
              "Helytelen bejelentkezési adatok",
              style: AppTheme.normal15TextStyle,
            ),
            behavior: SnackBarBehavior.floating,
          ));
        }
      },
      child: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Scaffold(
          backgroundColor: AppColor.backgroundColor,
          body: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: getPadding(left: 25.psw, right: 25.psw),
                  child: Text(
                    "Az alkalmazás használatához kérlek jelentkezz be",
                    textAlign: TextAlign.center,
                    style: AppTheme.normal25TextStyle,
                  ),
                ),
                SizedBox(
                  height: 70.psh,
                ),
                BlocBuilder<LoginBloc, LoginState>(
                  bloc: loginBloc,
                  builder: (context, state) {
                    bool isButtonsEnabled = true;
                    if (state is LoginLoadingState) {
                      isButtonsEnabled = false;
                    }
                    return Column(children: [
                      CustomTextInputField(
                        textEditingController: emailEditingController,
                        hintText: "Felhasználónév",
                        borderColor: (state is LoginErrorState)
                            ? AppColor.red
                            : AppColor.green,
                        prefixIcon: Icon(Icons.person),
                        enabled: isButtonsEnabled,
                        maxLines: 1,
                        onTap: () => loginBloc.add(ResetLoginEvent()),
                      ),
                      const SizedBox(height: 12),
                      CustomTextInputField(
                          textEditingController: passwordEditingController,
                          borderColor: (state is LoginErrorState)
                              ? AppColor.red
                              : AppColor.green,
                          obscureText: true,
                          hintText: "Jelszó",
                          prefixIcon: Icon(Icons.lock),
                          enabled: isButtonsEnabled,
                          textInputAction: TextInputAction.done,
                          onFieldSubmitted: (val) {
                            loginBloc.add(LoginUserEvent(
                                email: emailEditingController.text,
                                password: passwordEditingController.text));
                          },
                          maxLines: 1,
                          onTap: () => loginBloc.add(ResetLoginEvent())),
                      SizedBox(height: 25.psh),
                      (state is LoginLoadingState)
                          ? Align(
                              alignment: Alignment.center,
                              child: Padding(
                                  padding: getPadding(top: 13, bottom: 13),
                                  child: CircularProgressIndicator()))
                          : CustomTextButton(
                              text: "Bejelentkezés",
                              onTap: state is LoginSuccessState ||
                                      !isButtonsEnabled
                                  ? null
                                  : () {
                                      FocusScope.of(context).unfocus();
                                      loginBloc.add(LoginUserEvent(
                                          email: emailEditingController.text,
                                          password:
                                              passwordEditingController.text));
                                    })
                    ]);
                  },
                ),
                SizedBox(height: 30.psh),
              ]),
        ),
      ),
    );
  }
}
