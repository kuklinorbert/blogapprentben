import 'package:blogapprentben/features/presentation/bloc/blog/add_blog/add_blog_bloc.dart';
import 'package:blogapprentben/features/presentation/widgets/custom_text_button.dart';
import 'package:blogapprentben/features/presentation/widgets/custom_text_input_field.dart';
import 'package:blogapprentben/util/app_color.dart';
import 'package:blogapprentben/util/app_theme.dart';
import 'package:blogapprentben/util/extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../injection_container.dart';

class AddBlogPage extends StatefulWidget {
  AddBlogPage({
    super.key,
  });

  @override
  State<AddBlogPage> createState() => _AddBlogPageState();
}

class _AddBlogPageState extends State<AddBlogPage> {
  AddBlogBloc addBlogBloc = sl<AddBlogBloc>();

  bool isComplete = false;

  final TextEditingController titleController = TextEditingController();
  final TextEditingController contentController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AddBlogBloc, AddBlogState>(
      bloc: addBlogBloc,
      listener: (context, state) {
        if (state is AddBlogSuccess) {
          Navigator.pop(context);
        }
      },
      builder: (context, state) {
        return GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              child: SizedBox(
                height: 750.psh,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(height: 12.psh),
                    if (MediaQuery.of(context).viewInsets.bottom == 0.0)
                      Container(
                        width: 30.psw,
                        height: 3.psh,
                        decoration: BoxDecoration(
                            color: AppColor.lightGrey,
                            borderRadius: BorderRadius.circular(15)),
                      ),
                    Padding(
                      padding: EdgeInsets.only(
                          left: 20.psw, bottom: 10.psh, top: 28.psh),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Új blogposzt létrehozása",
                          style: AppTheme.bolder25TextStyle,
                        ),
                      ),
                    ),
                    SizedBox(height: 55.psh),
                    CustomTextInputField(
                      hintText: "Cím",
                      textEditingController: titleController,
                      onChanged: (value) => checkIfCompleted(),
                    ),
                    SizedBox(
                      height: 30.psh,
                    ),
                    CustomTextInputField(
                      hintText: "Tartalom",
                      maxLines: 12,
                      textEditingController: contentController,
                      onChanged: (value) => checkIfCompleted(),
                    ),
                    Spacer(),
                    Padding(
                      padding: getPadding(bottom: 40.psh),
                      child: CustomTextButton(
                        text: "Létrehozás",
                        onTap: isComplete
                            ? () {
                                addBlogBloc.add(AddNewBlogEvent(
                                    title: titleController.text,
                                    content: contentController.text));
                              }
                            : null,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  void checkIfCompleted() {
    if (titleController.text != "" && contentController.text != "") {
      isComplete = true;
    } else {
      isComplete = false;
    }
    setState(() {});
  }
}
