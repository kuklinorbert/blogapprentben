import 'package:blogapprentben/features/domain/entities/blog_data.dart';
import 'package:blogapprentben/features/domain/entities/user_data.dart';
import 'package:blogapprentben/features/presentation/bloc/blog/update_blog/update_blog_bloc.dart';
import 'package:blogapprentben/features/presentation/bloc/user_list/user_list_bloc.dart';
import 'package:blogapprentben/features/presentation/widgets/custom_text_button.dart';
import 'package:blogapprentben/features/presentation/widgets/custom_text_input_field.dart';
import 'package:blogapprentben/features/presentation/widgets/retry_button.dart';
import 'package:blogapprentben/features/presentation/widgets/user_selector_dropdown.dart';
import 'package:blogapprentben/injection_container.dart';
import 'package:blogapprentben/util/app_color.dart';
import 'package:blogapprentben/util/app_theme.dart';
import 'package:blogapprentben/util/extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UpdateBlogPage extends StatefulWidget {
  UpdateBlogPage({
    super.key,
    required this.blogData,
    required this.userData,
  });

  final BlogData blogData;
  final UserData userData;

  @override
  State<UpdateBlogPage> createState() => _UpdateBlogPageState();
}

class _UpdateBlogPageState extends State<UpdateBlogPage> {
  UpdateBlogBloc updateBlogBloc = sl<UpdateBlogBloc>();
  UserListBloc userListBloc = sl<UserListBloc>();

  final TextEditingController titleTextController = TextEditingController();
  final TextEditingController contentTextController = TextEditingController();
  late int createdUserId;

  bool isModifed = false;

  @override
  void initState() {
    titleTextController.text = widget.blogData.title;
    contentTextController.text = widget.blogData.content;
    createdUserId = widget.blogData.createdById;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<UpdateBlogBloc, UpdateBlogState>(
      bloc: updateBlogBloc,
      listener: (context, state) {
        if (state is UpdateBlogSuccess) {
          Navigator.pop(context);
        }
      },
      builder: (context, state) {
        return GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              child: SizedBox(
                height: 750.psh,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(height: 12.psh),
                    if (MediaQuery.of(context).viewInsets.bottom == 0.0)
                      Container(
                        width: 30.psw,
                        height: 3.psh,
                        decoration: BoxDecoration(
                            color: AppColor.lightGrey,
                            borderRadius: BorderRadius.circular(15)),
                      ),
                    Padding(
                      padding: EdgeInsets.only(
                          left: 20.psw, bottom: 10.psh, top: 28.psh),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Blogposzt módosítás",
                          style: AppTheme.bolder25TextStyle,
                        ),
                      ),
                    ),
                    SizedBox(height: 55.psh),
                    CustomTextInputField(
                      hintText: "Cím",
                      onChanged: (value) => checkIfModified(),
                      textEditingController: titleTextController,
                    ),
                    SizedBox(
                      height: 30.psh,
                    ),
                    CustomTextInputField(
                      hintText: "Tartalom",
                      maxLines: 12,
                      onChanged: (value) => checkIfModified(),
                      textEditingController: contentTextController,
                    ),
                    if (widget.userData.isAdmin)
                      BlocBuilder<UserListBloc, UserListState>(
                        bloc: userListBloc,
                        builder: (context, state) {
                          if (state is UserListLoaded) {
                            return Padding(
                              padding: getPadding(top: 35.psh),
                              child: UserSelector(
                                  currentUserId: createdUserId,
                                  userList: state.userList,
                                  onUserSelected: (newUser) {
                                    setState(() {
                                      createdUserId = newUser.id;
                                    });
                                    checkIfModified();
                                  }),
                            );
                          }
                          if (state is UserListError) {
                            RetryButton(
                                errorText:
                                    "Nem sikerült betölteni a felhasználókat.",
                                onPressed: () =>
                                    userListBloc.add(GetUserListEvent()));
                          }
                          return Center(
                              child: SizedBox(
                                  width: 50.psh,
                                  height: 50.psh,
                                  child: const CircularProgressIndicator(
                                    strokeWidth: 4,
                                  )));
                        },
                      ),
                    Spacer(),
                    Padding(
                      padding: getPadding(bottom: 40.psh),
                      child: CustomTextButton(
                        text: "Módosítás",
                        onTap: isModifed
                            ? () {
                                updateBlogBloc.add(UpdateBlogItemEvent(
                                    blogData: BlogData(
                                        id: widget.blogData.id,
                                        createdDate:
                                            widget.blogData.createdDate,
                                        createdById: createdUserId,
                                        createdUserName: '',
                                        title: titleTextController.text,
                                        content: contentTextController.text)));
                              }
                            : null,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  void checkIfModified() {
    if ((titleTextController.text != widget.blogData.title &&
            titleTextController.text != "") ||
        (contentTextController.text != widget.blogData.content &&
            contentTextController.text != "") ||
        createdUserId != widget.blogData.createdById) {
      isModifed = true;
    } else {
      isModifed = false;
    }
    setState(() {});
  }
}
