import 'package:animated_bottom_navigation_bar/animated_bottom_navigation_bar.dart';
import 'package:blogapprentben/features/domain/entities/user_data.dart';
import 'package:blogapprentben/features/presentation/bloc/blog/add_blog/add_blog_bloc.dart';
import 'package:blogapprentben/features/presentation/bloc/blog/blog_list/blog_list_bloc.dart';
import 'package:blogapprentben/features/presentation/bloc/blog/delete_blog/delete_blog_bloc.dart';
import 'package:blogapprentben/features/presentation/bloc/blog/update_blog/update_blog_bloc.dart';
import 'package:blogapprentben/features/presentation/bloc/login/login_bloc.dart';
import 'package:blogapprentben/features/presentation/bloc/user_data/user_data_bloc.dart';
import 'package:blogapprentben/features/presentation/bloc/user_list/user_list_bloc.dart';
import 'package:blogapprentben/features/presentation/screen/home/home_page.dart';
import 'package:blogapprentben/features/presentation/screen/profile/profile_page.dart';
import 'package:blogapprentben/features/presentation/widgets/bottom_sheet.dart';
import 'package:blogapprentben/features/presentation/widgets/exit_dialog.dart';
import 'package:blogapprentben/injection_container.dart';
import 'package:blogapprentben/util/app_color.dart';
import 'package:blogapprentben/util/app_key.dart';
import 'package:blogapprentben/util/app_theme.dart';
import 'package:blogapprentben/util/extension.dart';
import 'package:blogapprentben/util/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({super.key});

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int activeIndex = 0;
  PageController pageController = PageController();
  BlogListBloc blogListBloc = sl<BlogListBloc>();
  LoginBloc loginBloc = sl<LoginBloc>();
  UserDataBloc userDataBloc = sl<UserDataBloc>();
  AddBlogBloc addBlogBloc = sl<AddBlogBloc>();
  DeleteBlogBloc deleteBlogBloc = sl<DeleteBlogBloc>();
  UpdateBlogBloc updateBlogBloc = sl<UpdateBlogBloc>();
  UserListBloc userListBloc = sl<UserListBloc>();

  @override
  void initState() {
    userDataBloc.add(GetUserDataEvent());
    blogListBloc.add(LoadBlogListEvent());
    userListBloc.add(GetUserListEvent());

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);

    return MultiBlocListener(
      listeners: [
        BlocListener<LoginBloc, LoginState>(
          bloc: loginBloc,
          listener: (context, state) {
            if (state is LogoutSuccessState) {
              userDataBloc.add(ResetUserDataEvent());
              Navigator.pushNamedAndRemoveUntil(
                  context, AppKey.routeLogin, (route) => false);
            }
          },
        ),
        BlocListener<AddBlogBloc, AddBlogState>(
          bloc: addBlogBloc,
          listener: (context, state) {
            if (state is AddBlogSuccess) {
              blogListBloc.add(LoadBlogListEvent());
              userDataBloc.add(GetUserDataEvent());
            }
          },
        ),
        BlocListener<UserDataBloc, UserDataState>(
          bloc: userDataBloc,
          listener: (context, state) {
            if (state is UserDataError) {
              print("USER ERROR");
              loginBloc.add(LogoutEvent());
            }
          },
        ),
        BlocListener<DeleteBlogBloc, DeleteBlogState>(
          bloc: deleteBlogBloc,
          listener: (context, state) {
            if (state is DeleteBlogSuccess) {
              userDataBloc.add(GetUserDataEvent());
              blogListBloc.add(LoadBlogListEvent());
            }
          },
        ),
        BlocListener<UpdateBlogBloc, UpdateBlogState>(
          bloc: updateBlogBloc,
          listener: (context, state) {
            if (state is UpdateBlogSuccess) {
              userDataBloc.add(GetUserDataEvent());
              blogListBloc.add(LoadBlogListEvent());
            }
          },
        ),
      ],
      child: WillPopScope(
        onWillPop: () async => askIfShouldExit(context),
        child: Scaffold(
          backgroundColor: AppColor.backgroundColor,
          appBar: AppBar(
            title: Text('Blog App RentBen'),
            elevation: 1,
          ),
          body: PageView(
              controller: pageController,
              onPageChanged: (index) => setState(() {
                    activeIndex = index;
                  }),
              physics: BouncingScrollPhysics(),
              children: [HomePage(), ProfilePage()]),
          floatingActionButton: MediaQuery.of(context).viewInsets.bottom != 0
              ? null
              : FloatingActionButton(
                  onPressed: () {
                    buildBottomSheet(context);
                  },
                  child: Icon(
                    Icons.add_rounded,
                    size: 30.psh,
                  ),
                ),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
          bottomNavigationBar: AnimatedBottomNavigationBar.builder(
            activeIndex: activeIndex,
            itemCount: 2,
            onTap: (index) {
              pageController.animateToPage(index,
                  duration: Duration(milliseconds: 300), curve: Curves.easeIn);
              setState(() {
                activeIndex = index;
              });
            },
            gapLocation: GapLocation.center,
            tabBuilder: (index, isActive) {
              if (index == 0) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Icon(
                      Icons.home,
                      size: 26.psh,
                      color: isActive ? AppColor.green : Colors.grey,
                    ),
                    Text(
                      "Kezdőlap",
                      style: AppTheme.normal12TextStyle.copyWith(
                          color: isActive ? AppColor.green : Colors.grey,
                          fontWeight:
                              isActive ? FontWeight.w500 : FontWeight.w400),
                    )
                  ],
                );
              }
              if (index == 1) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Icon(
                      Icons.person,
                      size: 26.psh,
                      color: isActive ? AppColor.green : Colors.grey,
                    ),
                    Text(
                      "Profil",
                      style: AppTheme.normal12TextStyle.copyWith(
                          color: isActive ? AppColor.green : Colors.grey,
                          fontWeight:
                              isActive ? FontWeight.w500 : FontWeight.w400),
                    )
                  ],
                );
              }
              return Container();
            },
            // icons: [Icons.home, Icons.person],

            // inactiveColor: Colors.grey,
            // activeColor: AppColor.green,
          ),
        ),
      ),
    );
  }
}
