import 'package:blogapprentben/features/domain/entities/user_data.dart';
import 'package:blogapprentben/features/presentation/bloc/blog/blog_list/blog_list_bloc.dart';
import 'package:blogapprentben/features/presentation/bloc/user_data/user_data_bloc.dart';
import 'package:blogapprentben/features/presentation/widgets/blog_list_view.dart';
import 'package:blogapprentben/features/presentation/widgets/retry_button.dart';
import 'package:blogapprentben/injection_container.dart';
import 'package:blogapprentben/util/extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with AutomaticKeepAliveClientMixin {
  BlogListBloc blogListBloc = sl<BlogListBloc>();
  UserDataBloc userDataBloc = sl<UserDataBloc>();

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder<UserDataBloc, UserDataState>(
      bloc: userDataBloc,
      builder: (context, state) {
        UserData? userData;
        final userDataState = userDataBloc.state;
        if (userDataState is UserDataLoaded) {
          userData = userDataState.userData;
        }
        return BlocBuilder<BlogListBloc, BlogListState>(
          bloc: blogListBloc,
          builder: (context, state) {
            if (state is BlogListLoaded) {
              return BlogListView(
                blogList: state.blogList,
                userData: userData,
              );
            }
            if (state is BlogListError) {
              return Center(
                child: RetryButton(
                  errorText: "Nem sikerült betölteni a blogokat.",
                  onPressed: () => blogListBloc.add(LoadBlogListEvent()),
                ),
              );
            }
            return Center(
                child: SizedBox(
                    width: 50.psh,
                    height: 50.psh,
                    child: const CircularProgressIndicator(
                      strokeWidth: 4,
                    )));
          },
        );
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}
