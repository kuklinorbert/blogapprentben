import 'package:blogapprentben/features/domain/entities/blog_data.dart';
import 'package:blogapprentben/features/domain/entities/user_data.dart';
import 'package:blogapprentben/features/presentation/widgets/blog_item_widget.dart';
import 'package:blogapprentben/util/extension.dart';
import 'package:flutter/material.dart';

class BlogListView extends StatefulWidget {
  const BlogListView(
      {super.key,
      required this.blogList,
      required this.userData,
      this.shrinkWrap = false});

  final List<BlogData> blogList;
  final UserData? userData;
  final bool shrinkWrap;

  @override
  State<BlogListView> createState() => _BlogListViewState();
}

class _BlogListViewState extends State<BlogListView> {
  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      physics: BouncingScrollPhysics(),
      padding: getPadding(top: 20.psh, bottom: 30.psh),
      itemCount: widget.blogList.length,
      shrinkWrap: widget.shrinkWrap,
      itemBuilder: (BuildContext context, int index) {
        final blogItem = widget.blogList[index];
        return BlogItemWidget(
          blogItem: blogItem,
          userData: widget.userData,
        );
      },
      separatorBuilder: (BuildContext context, int index) {
        return SizedBox(
          height: 10.psh,
        );
      },
    );
  }
}
