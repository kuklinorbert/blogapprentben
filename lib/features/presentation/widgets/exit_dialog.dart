import 'package:blogapprentben/util/app_theme.dart';
import 'package:blogapprentben/util/extension.dart';
import 'package:flutter/material.dart';

Future<bool> askIfShouldExit(BuildContext context) {
  return showDialog(
    context: context,
    builder: (context) {
      return ExitDialog();
    },
  ).then((value) => value);
}

class ExitDialog extends StatelessWidget {
  const ExitDialog({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: EdgeInsets.only(top: 10),
      scrollable: true,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(14)),
      content: SizedBox(
          width: 270.psw,
          child: Center(
              child: Column(
            children: [
              Padding(
                padding:
                    EdgeInsets.only(left: 20.psw, right: 20.psw, top: 5.psh),
                child: Text(
                  "Kilépés",
                  textAlign: TextAlign.center,
                  style: AppTheme.bolder20TextStyle,
                ),
              ),
              SizedBox(height: 15.psh),
              Padding(
                padding: EdgeInsets.only(left: 20.psw, right: 20.psw),
                child: Text(
                  "Biztosan ki szeretne lépni?",
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 20.psh),
              const Divider(
                height: 1,
                thickness: 1,
              ),
              IntrinsicHeight(
                child: Row(
                  children: [
                    Expanded(
                        child: SizedBox(
                      height: 45.psh,
                      child: TextButton(
                          style: TextButton.styleFrom(
                            minimumSize: Size.fromHeight(45),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(14))),
                          ),
                          onPressed: () {
                            Navigator.pop(context, false);
                          },
                          child: Text(
                            "Mégsem",
                          )),
                    )),
                    const VerticalDivider(
                      width: 1,
                      thickness: 1,
                    ),
                    Expanded(
                      child: SizedBox(
                        height: 45.psh,
                        child: TextButton(
                            style: TextButton.styleFrom(
                              minimumSize: Size.fromHeight(45),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(14))),
                            ),
                            onPressed: () => Navigator.pop(context, true),
                            child: Text(
                              "Kilépés",
                            )),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ))),
    );
  }
}
