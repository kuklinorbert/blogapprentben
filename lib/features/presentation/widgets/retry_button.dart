import 'package:blogapprentben/util/app_theme.dart';
import 'package:blogapprentben/util/extension.dart';
import 'package:flutter/material.dart';

class RetryButton extends StatelessWidget {
  const RetryButton({
    super.key,
    required this.onPressed,
    required this.errorText,
  });

  final String errorText;
  final void Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Padding(
        padding: getPadding(left: 20.psw, right: 20.psw),
        child: Text(
          errorText,
          textAlign: TextAlign.center,
          style: AppTheme.normal20TextStyle,
        ),
      ),
      SizedBox(height: 20.psh),
      TextButton.icon(
          onPressed: onPressed,
          icon: Icon(
            Icons.refresh,
          ),
          label: Text(
            "Újrapróbálkozás",
          ))
    ]);
  }
}
