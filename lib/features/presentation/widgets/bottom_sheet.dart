import 'package:blogapprentben/features/domain/entities/blog_data.dart';
import 'package:blogapprentben/features/domain/entities/user_data.dart';
import 'package:blogapprentben/features/presentation/screen/blog/add_blog/add_blog_page.dart';
import 'package:blogapprentben/features/presentation/screen/blog/update_blog/update_blog_page.dart';
import 'package:blogapprentben/util/app_color.dart';
import 'package:flutter/material.dart';

Future<dynamic> buildBottomSheet(BuildContext context,
    [bool isModify = false, BlogData? blogData, UserData? userData]) {
  return showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(13.0),
          topRight: Radius.circular(13.0),
        ),
      ),
      enableDrag: true,
      isDismissible: true,
      isScrollControlled: true,
      backgroundColor: AppColor.backgroundColor,
      builder: (context) {
        if (isModify && blogData != null && userData != null) {
          return UpdateBlogPage(
            blogData: blogData,
            userData: userData,
          );
        } else {
          return AddBlogPage();
        }
      });
}
