import 'package:blogapprentben/features/data/datasources/user_data_source.dart';
import 'package:blogapprentben/features/domain/entities/user_data.dart';
import 'package:blogapprentben/util/app_color.dart';
import 'package:blogapprentben/util/app_theme.dart';
import 'package:blogapprentben/util/extension.dart';
import 'package:flutter/material.dart';

class UserSelector extends StatelessWidget {
  UserSelector({
    Key? key,
    required this.currentUserId,
    required this.userList,
    required this.onUserSelected,
  }) : super(key: key);

  final int currentUserId;
  final List<UserData> userList;
  final void Function(UserData) onUserSelected;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: getPadding(left: 15.psw, right: 15.psw),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "Létrehozta:",
            style: AppTheme.normal20TextStyle,
          ),
          SizedBox(
            width: 40.psw,
          ),
          GestureDetector(
            onTap: () async {
              await showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    contentPadding: EdgeInsets.only(top: 10),
                    scrollable: true,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(14)),
                    content: SizedBox(
                        width: 270.psw,
                        height: 400.psh,
                        child: Center(
                            child: Column(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(
                                  left: 20.psw, right: 20.psw, top: 10.psh),
                              child: Text(
                                "Válassz felhasználót",
                                textAlign: TextAlign.center,
                                style: AppTheme.normal20TextStyle,
                              ),
                            ),
                            SizedBox(height: 15.psh),
                            const Divider(
                              height: 1,
                              thickness: 1,
                            ),
                            Expanded(
                              child: ListView.builder(
                                physics: BouncingScrollPhysics(),
                                itemCount: userDataList.length,
                                padding: getPadding(top: 10.psh),
                                itemBuilder: (BuildContext context, int index) {
                                  final currentItem = userDataList[index];
                                  return ListTile(
                                    leading: currentItem.id == currentUserId
                                        ? Icon(Icons.check,
                                            color: AppColor.green, size: 28)
                                        : SizedBox(
                                            width: 30.psw,
                                          ),
                                    title: Text(
                                      currentItem.name,
                                      style: currentItem.id == currentUserId
                                          ? AppTheme.bold15TextStyle
                                          : AppTheme.normal15TextStyle,
                                    ),
                                    onTap: () {
                                      onUserSelected(currentItem);
                                      Navigator.pop(context);
                                    },
                                  );
                                },
                              ),
                            )
                          ],
                        ))),
                  );
                },
              );
            },
            child: Container(
              height: 65.psh,
              width: 165.psw,
              decoration: BoxDecoration(
                color: AppColor.textFieldFillColor,
                borderRadius: BorderRadius.circular(4),
              ),
              child: Center(
                child: Text(
                  userList
                      .firstWhere((element) => element.id == currentUserId)
                      .name,
                  style: AppTheme.bold15TextStyle,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
