import 'package:auto_size_text/auto_size_text.dart';
import 'package:blogapprentben/features/domain/entities/blog_data.dart';
import 'package:blogapprentben/features/domain/entities/user_data.dart';
import 'package:blogapprentben/features/presentation/bloc/blog/delete_blog/delete_blog_bloc.dart';
import 'package:blogapprentben/features/presentation/widgets/bottom_sheet.dart';
import 'package:blogapprentben/injection_container.dart';
import 'package:blogapprentben/util/app_color.dart';
import 'package:blogapprentben/util/app_theme.dart';
import 'package:blogapprentben/util/extension.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class BlogItemWidget extends StatefulWidget {
  const BlogItemWidget({
    super.key,
    required this.blogItem,
    required this.userData,
  });

  final BlogData blogItem;
  final UserData? userData;

  @override
  State<BlogItemWidget> createState() => _BlogItemWidgetState();
}

class _BlogItemWidgetState extends State<BlogItemWidget> {
  DeleteBlogBloc deleteBlogBloc = sl<DeleteBlogBloc>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding:
            getPadding(top: 20.psh, bottom: 20.psh, left: 15.psw, right: 5.psw),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 50.psh,
                child: Row(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: AutoSizeText(
                          widget.blogItem.title.replaceAll("", "\u{200B}"),
                          minFontSize: 22,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style: AppTheme.bolder35TextStyle,
                        ),
                      ),
                      if (widget.userData != null &&
                          (widget.userData!.isAdmin ||
                              widget.userData!.id ==
                                  widget.blogItem.createdById))
                        PopupMenuButton<int>(
                          onSelected: (item) {
                            if (item == 0) {
                              buildBottomSheet(context, true, widget.blogItem,
                                  widget.userData);
                            }
                            if (item == 1) {
                              deleteBlogBloc.add(DeleteBlogByIdEvent(
                                  blogId: widget.blogItem.id));
                            }
                          },
                          itemBuilder: (context) => [
                            PopupMenuItem<int>(
                                value: 0, child: Text('Módosítás')),
                            if (widget.userData!.isAdmin)
                              PopupMenuItem<int>(
                                  value: 1,
                                  child: Text(
                                    'Törlés',
                                    style: TextStyle(color: AppColor.red),
                                  )),
                          ],
                        ),
                    ]),
              ),
              SizedBox(
                height: 20.psh,
              ),
              Text(
                widget.blogItem.content,
                style: AppTheme.normal15TextStyle,
              ),
              SizedBox(
                height: 25.psh,
              ),
              Row(
                children: [
                  Text(
                    widget.blogItem.createdUserName,
                    style: TextStyle(color: AppColor.lightGrey),
                  ),
                  Spacer(),
                  Text(
                    DateFormat('MMM dd - HH:mm', 'hu').format(
                      widget.blogItem.createdDate,
                    ),
                    style: TextStyle(color: AppColor.lightGrey),
                  )
                ],
              )
            ]),
      ),
    );
  }
}
