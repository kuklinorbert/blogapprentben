import 'package:blogapprentben/util/app_color.dart';
import 'package:blogapprentben/util/app_theme.dart';
import 'package:blogapprentben/util/extension.dart';
import 'package:flutter/material.dart';

class CustomTextButton extends StatelessWidget {
  const CustomTextButton({this.text, this.onTap, this.width, super.key});

  final String? text;
  final VoidCallback? onTap;
  final double? width;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        style: _buildTextButtonStyle(),
        onPressed: onTap,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              text ?? "",
              textAlign: TextAlign.center,
              style: AppTheme.normal18TextStyle.copyWith(
                  color: onTap != null ? Colors.white : AppColor.lightGrey),
            ),
          ],
        ),
      ),
    );
  }

  _buildTextButtonStyle() {
    return TextButton.styleFrom(
        fixedSize: Size(width ?? 345.psw, 60.psh),
        backgroundColor: AppColor.green,
        disabledBackgroundColor: AppColor.lighterGrey,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.psh),
        )).copyWith(
      overlayColor: MaterialStateProperty.resolveWith(
          (state) => AppColor.overlayDarkGreen.withOpacity(0.75)),
    );
  }
}
