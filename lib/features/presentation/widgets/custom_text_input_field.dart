import 'package:blogapprentben/util/app_color.dart';
import 'package:blogapprentben/util/extension.dart';
import 'package:flutter/material.dart';

class CustomTextInputField extends StatelessWidget {
  const CustomTextInputField({
    super.key,
    this.textEditingController,
    this.onFieldSubmitted,
    this.onChanged,
    this.onTap,
    this.borderColor = AppColor.green,
    this.hintText,
    this.obscureText = false,
    this.textInputAction,
    this.prefixIcon,
    this.enabled,
    this.maxLines = 1,
  });

  final TextEditingController? textEditingController;
  final void Function(String)? onFieldSubmitted;
  final void Function(String)? onChanged;
  final void Function()? onTap;
  final Color borderColor;
  final String? hintText;
  final bool obscureText;
  final Widget? prefixIcon;
  final TextInputAction? textInputAction;
  final bool? enabled;
  final int? maxLines;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 345.psw,
      child: TextFormField(
          controller: textEditingController,
          enabled: enabled,
          decoration: InputDecoration(
            hintText: hintText,
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: borderColor, width: 1.5),
              borderRadius: BorderRadius.circular(10),
            ),
            filled: true,
            fillColor: AppColor.textFieldFillColor,
            border: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.circular(10)),
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide:
                    BorderSide(color: AppColor.borderGreen, width: 1.5)),
            focusedErrorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(color: AppColor.red, width: 1.5)),
            errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(color: AppColor.red, width: 1.0)),
            disabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide:
                    BorderSide(color: AppColor.borderGreen, width: 1.5)),
            prefixIcon: prefixIcon,
          ),
          obscureText: obscureText,
          textInputAction: textInputAction,
          maxLines: maxLines,
          onChanged: onChanged,
          onTap: onTap,
          onFieldSubmitted: onFieldSubmitted),
    );
  }
}
