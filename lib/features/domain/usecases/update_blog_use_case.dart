import 'package:blogapprentben/core/failures.dart';
import 'package:blogapprentben/core/usecases.dart';
import 'package:blogapprentben/features/data/repositories/blog_repository.dart';
import 'package:blogapprentben/features/domain/entities/blog_data.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

class UpdateBlogUseCase extends UseCase<bool, UpdateBlogParams> {
  final BlogRepository blogRepository;

  UpdateBlogUseCase(this.blogRepository);

  @override
  Future<Either<Failure, bool>> call(UpdateBlogParams params) async {
    return await blogRepository.updateBlog(params.blogData);
  }
}

class UpdateBlogParams extends Equatable {
  final BlogData blogData;

  const UpdateBlogParams({required this.blogData});

  @override
  List<Object> get props => [blogData];
}
