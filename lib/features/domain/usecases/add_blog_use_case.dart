import 'package:blogapprentben/core/failures.dart';
import 'package:blogapprentben/core/usecases.dart';
import 'package:blogapprentben/features/data/repositories/blog_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

class AddBlogUseCase extends UseCase<bool, AddBlogParams> {
  final BlogRepository blogRepository;

  AddBlogUseCase(this.blogRepository);

  @override
  Future<Either<Failure, bool>> call(AddBlogParams params) async {
    return await blogRepository.addBlog(params.title, params.content);
  }
}

class AddBlogParams extends Equatable {
  final String title;
  final String content;

  const AddBlogParams({required this.title, required this.content});

  @override
  List<Object> get props => [title, content];
}
