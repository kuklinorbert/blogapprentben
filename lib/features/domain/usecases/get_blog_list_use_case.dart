import 'package:blogapprentben/core/failures.dart';
import 'package:blogapprentben/core/usecases.dart';
import 'package:blogapprentben/features/data/repositories/blog_repository.dart';
import 'package:blogapprentben/features/domain/entities/blog_data.dart';
import 'package:dartz/dartz.dart';

class GetBlogListUseCase extends UseCase<List<BlogData>, NoParams> {
  final BlogRepository blogRepository;

  GetBlogListUseCase(this.blogRepository);

  @override
  Future<Either<Failure, List<BlogData>>> call(NoParams noParams) async {
    return await blogRepository.getBlogList();
  }
}
