import 'package:blogapprentben/core/failures.dart';
import 'package:blogapprentben/core/usecases.dart';
import 'package:blogapprentben/features/data/repositories/auth_repository.dart';
import 'package:dartz/dartz.dart';

class LogoutUseCase implements UseCase<bool, NoParams> {
  final AuthRepository authRepository;

  LogoutUseCase(this.authRepository);

  @override
  Future<Either<Failure, bool>> call(NoParams params) async {
    return await authRepository.logout();
  }
}
