import 'package:blogapprentben/core/failures.dart';
import 'package:blogapprentben/core/usecases.dart';
import 'package:blogapprentben/features/data/repositories/user_repository.dart';
import 'package:blogapprentben/features/domain/entities/user_data.dart';
import 'package:dartz/dartz.dart';

class GetUserDataUseCase extends UseCase<UserData, NoParams> {
  final UserRepository userRepository;

  GetUserDataUseCase(this.userRepository);

  @override
  Future<Either<Failure, UserData>> call(NoParams noParams) async {
    return await userRepository.getUserData();
  }
}
