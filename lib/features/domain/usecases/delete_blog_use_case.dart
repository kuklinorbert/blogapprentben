import 'package:blogapprentben/core/failures.dart';
import 'package:blogapprentben/core/usecases.dart';
import 'package:blogapprentben/features/data/repositories/blog_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

class DeleteBlogUseCase extends UseCase<bool, DeleteBlogParams> {
  final BlogRepository blogRepository;

  DeleteBlogUseCase(this.blogRepository);

  @override
  Future<Either<Failure, bool>> call(DeleteBlogParams params) async {
    return await blogRepository.deleteBlog(params.blogId);
  }
}

class DeleteBlogParams extends Equatable {
  final int blogId;

  const DeleteBlogParams({required this.blogId});

  @override
  List<Object> get props => [blogId];
}
