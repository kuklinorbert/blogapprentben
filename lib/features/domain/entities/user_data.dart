import 'package:blogapprentben/features/domain/entities/blog_data.dart';
import 'package:equatable/equatable.dart';

class UserData extends Equatable {
  final int id;
  final bool isAdmin;
  final String name;
  List<BlogData> userBlogs;

  UserData({
    required this.id,
    required this.isAdmin,
    required this.name,
    this.userBlogs = const [],
  });

  @override
  List<Object?> get props => [id, isAdmin, name, userBlogs];
}
