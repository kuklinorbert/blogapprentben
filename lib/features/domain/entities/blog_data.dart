import 'package:equatable/equatable.dart';

class BlogData extends Equatable {
  final int id;
  final int createdById;
  String createdUserName;
  final DateTime createdDate;
  final String title;
  final String content;

  BlogData(
      {required this.id,
      required this.createdDate,
      required this.createdById,
      required this.createdUserName,
      required this.title,
      required this.content});

  @override
  List<Object?> get props => [
        id,
        createdById,
        createdUserName,
        createdDate,
        title,
        content,
      ];
}
