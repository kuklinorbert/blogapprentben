import 'package:blogapprentben/core/failures.dart';
import 'package:blogapprentben/features/data/datasources/blog_data_source.dart';
import 'package:blogapprentben/features/data/datasources/user_data_source.dart';
import 'package:blogapprentben/features/domain/entities/blog_data.dart';
import 'package:dartz/dartz.dart';

abstract class BlogRepository {
  Future<Either<Failure, List<BlogData>>> getBlogList();
  Future<Either<Failure, bool>> addBlog(String title, String content);
  Future<Either<Failure, bool>> deleteBlog(int blogId);
  Future<Either<Failure, bool>> updateBlog(BlogData blogData);
}

class BlogRepositoryImpl implements BlogRepository {
  final UserDataSource userDataSource;
  final BlogDataSource blogDataSource;

  BlogRepositoryImpl(
      {required this.userDataSource, required this.blogDataSource});

  @override
  Future<Either<Failure, List<BlogData>>> getBlogList() async {
    try {
      final blogList = await blogDataSource.getBlogList();
      for (var blog in blogList) {
        blog.createdUserName =
            userDataSource.getUserDataById(blog.createdById)?.name ?? "";
      }
      return Right(blogList);
    } catch (e, stacktrace) {
      print("Get blog list exception: ${e.toString()}");
      print(stacktrace.toString());
      return Left(UnexpectedFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> addBlog(String title, String content) async {
    try {
      final userId = userDataSource.getUserId();
      final response =
          await blogDataSource.saveBlog(userId, DateTime.now(), title, content);
      if (response >= 0) {
        return const Right(true);
      } else {
        return Left(BlogFailure());
      }
    } catch (e, stacktrace) {
      print("Add blog exception: ${e.toString()}");
      print(stacktrace.toString());
      return Left(UnexpectedFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> deleteBlog(int blogId) async {
    try {
      final response = await blogDataSource.deleteBlog(blogId);
      return Right(response);
    } catch (e, stacktrace) {
      print("Add delete exception: ${e.toString()}");
      print(stacktrace.toString());
      return Left(UnexpectedFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> updateBlog(BlogData blogData) async {
    try {
      final response = await blogDataSource.updateBlog(blogData);
      return Right(response);
    } catch (e, stacktrace) {
      print("Update blog exception: ${e.toString()}");
      print(stacktrace.toString());
      return Left(UnexpectedFailure());
    }
  }
}
