import 'package:blogapprentben/core/failures.dart';
import 'package:blogapprentben/features/data/datasources/blog_data_source.dart';
import 'package:blogapprentben/features/data/datasources/user_data_source.dart';
import 'package:dartz/dartz.dart';

import '../../domain/entities/user_data.dart';

abstract class UserRepository {
  Future<Either<Failure, UserData>> getUserData();
  Future<Either<Failure, List<UserData>>> getUserList();
}

class UserRepositoryImpl implements UserRepository {
  final UserDataSource userDataSource;
  final BlogDataSource blogDataSource;

  UserRepositoryImpl({
    required this.userDataSource,
    required this.blogDataSource,
  });

  @override
  Future<Either<Failure, UserData>> getUserData() async {
    try {
      final userId = userDataSource.getUserId();
      var userData = userDataSource.getUserDataById(userId);
      if (userData != null) {
        userData.userBlogs = await blogDataSource.getUserBlogs(userId);
        return Right(userData);
      } else {
        return Left(UserFailure());
      }
    } catch (e, stacktrace) {
      print("getUserData exception: ${e.toString()}");
      print(stacktrace.toString());
      return Left(UnexpectedFailure());
    }
  }

  @override
  Future<Either<Failure, List<UserData>>> getUserList() async {
    try {
      var userList = userDataSource.getUserList();
      return Right(userList);
    } catch (e, stacktrace) {
      print("getUserList exception: ${e.toString()}");
      print(stacktrace.toString());
      return Left(UnexpectedFailure());
    }
  }
}
