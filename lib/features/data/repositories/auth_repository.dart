import 'package:blogapprentben/core/failures.dart';
import 'package:blogapprentben/features/data/datasources/auth_data_source.dart';
import 'package:blogapprentben/features/data/datasources/user_data_source.dart';
import 'package:dartz/dartz.dart';

abstract class AuthRepository {
  Future<Either<Failure, bool>> login(String email, String password);
  Future<Either<Failure, bool>> logout();
}

class AuthRepositoryImpl implements AuthRepository {
  final AuthDataSource authDataSource;
  final UserDataSource userDataSource;

  AuthRepositoryImpl(
      {required this.authDataSource, required this.userDataSource});

  @override
  Future<Either<Failure, bool>> login(String email, String password) async {
    try {
      final response = await authDataSource.login(email, password);
      if (response != -1) {
        await userDataSource.saveUserData(response);
        return const Right(true);
      } else {
        return Left(LoginFailure());
      }
    } catch (e, stacktrace) {
      print("login exception: ${e.toString()}");
      print(stacktrace.toString());
      return Left(UnexpectedFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> logout() async {
    try {
      final userId = userDataSource.getUserId();
      final result = await authDataSource.logout(userId);
      return Right(result);
    } catch (e, stacktrace) {
      print("logout exception: ${e.toString()}");
      print(stacktrace.toString());
      return Left(UnexpectedFailure());
    }
  }
}
