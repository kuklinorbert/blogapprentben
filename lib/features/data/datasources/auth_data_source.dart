import 'package:blogapprentben/util/app_key.dart';
import 'package:hive/hive.dart';

abstract class AuthDataSource {
  Future<int> login(String email, String password);
  Future<bool> logout(int userId);
}

class AuthDataSourceImpl implements AuthDataSource {
  AuthDataSourceImpl();

  @override
  Future<int> login(String email, String password) async {
    await Future.delayed(Duration(milliseconds: 1500));
    if (email == "admin" && password == "admin") {
      return 1;
    }
    if (email == "user1" && password == "test") {
      return 2;
    }
    if (email == "user2" && password == "test") {
      return 3;
    }
    if (email == "user3" && password == "test") {
      return 4;
    }
    return -1;
  }

  @override
  Future<bool> logout(int userId) async {
    final box = Hive.box(AppKey.hiveBoxUserData);
    await box.delete(AppKey.hiveKeyUserId);

    return true;
  }
}
