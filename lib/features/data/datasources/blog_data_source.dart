import 'package:blogapprentben/features/data/models/blog_model.dart';
import 'package:blogapprentben/features/domain/entities/blog_data.dart';
import 'package:blogapprentben/features/domain/entities/user_data.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:blogapprentben/util/app_key.dart';

abstract class BlogDataSource {
  Future<int> saveBlog(
      int createdById, DateTime createdDate, String title, String content);
  Future<bool> deleteBlog(int blogId);
  Future<bool> updateBlog(BlogData blogData);
  Future<List<BlogData>> getBlogList();
  Future<List<BlogData>> getUserBlogs(int userId);
}

class BlogDataSourceImpl implements BlogDataSource {
  BlogDataSourceImpl();

  @override
  Future<List<BlogData>> getBlogList() async {
    final box = Hive.box<BlogModel>(AppKey.hiveKeyBlogData);
    final blogEntries = box.toMap().entries;
    final blogList = blogEntries
        .map((e) => BlogData(
            id: e.key,
            createdDate: e.value.createdDate,
            createdById: e.value.createdById,
            createdUserName: '',
            title: e.value.title,
            content: e.value.content))
        .toList();

    blogList.sort((a, b) {
      return b.createdDate.compareTo(a.createdDate);
    });
    return blogList;
  }

  @override
  Future<int> saveBlog(int createdById, DateTime createdDate, String title,
      String content) async {
    final box = Hive.box<BlogModel>(AppKey.hiveKeyBlogData);
    final result = await box.add(BlogModel(
        createdById: createdById,
        createdDate: createdDate,
        title: title,
        content: content));
    return result;
  }

  @override
  Future<bool> updateBlog(BlogData blogData) async {
    final box = Hive.box<BlogModel>(AppKey.hiveKeyBlogData);

    box.put(
        blogData.id,
        BlogModel(
            createdById: blogData.createdById,
            createdDate: blogData.createdDate,
            title: blogData.title,
            content: blogData.content));
    return true;
  }

  @override
  Future<bool> deleteBlog(int blogId) async {
    final box = Hive.box<BlogModel>(AppKey.hiveKeyBlogData);
    await box.delete(blogId);
    return true;
  }

  @override
  Future<List<BlogData>> getUserBlogs(int userId) async {
    final box = Hive.box<BlogModel>(AppKey.hiveKeyBlogData);
    final blogEntries = box
        .toMap()
        .entries
        .where((element) => element.value.createdById == userId);

    final blogList = blogEntries
        .map((e) => BlogData(
            id: e.key,
            createdDate: e.value.createdDate,
            createdById: e.value.createdById,
            createdUserName: '',
            title: e.value.title,
            content: e.value.content))
        .toList();

    blogList.sort((a, b) {
      return b.createdDate.compareTo(a.createdDate);
    });
    return blogList;
  }
}
