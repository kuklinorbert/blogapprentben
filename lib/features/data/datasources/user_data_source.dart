import 'package:blogapprentben/features/domain/entities/user_data.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:blogapprentben/util/app_key.dart';
import 'package:collection/collection.dart';

abstract class UserDataSource {
  Future<void> saveUserData(int userId);
  int getUserId();
  UserData? getUserDataById(int userId);
  List<UserData> getUserList();
}

class UserDataSourceImpl implements UserDataSource {
  UserDataSourceImpl();

  @override
  Future<void> saveUserData(int userId) async {
    final box = Hive.box(AppKey.hiveBoxUserData);
    box.put(AppKey.hiveKeyUserId, userId);
  }

  @override
  int getUserId() {
    final box = Hive.box(AppKey.hiveBoxUserData);
    return box.get(AppKey.hiveKeyUserId);
  }

  @override
  UserData? getUserDataById(int userId) {
    final searchResult =
        userDataList.firstWhereOrNull((element) => element.id == userId);
    return searchResult;
  }

  @override
  List<UserData> getUserList() {
    return userDataList;
  }
}

List<UserData> userDataList = [
  UserData(id: 1, isAdmin: true, name: "Admin"),
  UserData(id: 2, isAdmin: false, name: "Test User 1"),
  UserData(id: 3, isAdmin: false, name: "Test User 2"),
  UserData(id: 4, isAdmin: false, name: "Test User 3")
];


// class LoginRepository {
//   Future<Either<Failure, Box>> getUsers() async {
//     try {
//       final box = Hive.box('users');
//       return Right(box);
//     } on Exception {
//       return Left(GetUsersFailure());
//     }
//   }

//   Future<Either<Failure, Box>> addUser(String name) async {
//     try {
//       final box = Hive.box('users');
//       var uuid = const Uuid();
//       User user = User(uid: uuid.v1(), name: name);
//       box.add(user);
//       return Right(box);
//     } on Exception {
//       return Left(AddUserFailure());
//     }
//   }

//   Future<Either<Failure, Box>> deleteUser(int index, String userId) async {
//     try {
//       final box = Hive.box('users');
//       box.deleteAt(index);
//       final box1 = Hive.box<Expense>('expenses');
//       var box1helper = box1.toMap();
//       box1helper.forEach((key, value) {
//         if (value.userId == userId) {
//           box1.delete(key);
//         }
//       });
//       final box2 = Hive.box<Income>('incomes');
//       var box2helper = box2.toMap();
//       box2helper.forEach((key, value) {
//         if (value.userId == userId) {
//           box2.delete(key);
//         }
//       });
//       return Right(box);
//     } on Exception {
//       return Left(DeleteUserFailure());
//     }
//   }
// }
