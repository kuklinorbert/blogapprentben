import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';

part 'blog_model.g.dart';

@HiveType(typeId: 0)
class BlogModel extends Equatable {
  @HiveField(0)
  final int createdById;

  @HiveField(1)
  final DateTime createdDate;

  @HiveField(2)
  final String title;

  @HiveField(3)
  final String content;

  const BlogModel({
    required this.createdById,
    required this.createdDate,
    required this.title,
    required this.content,
  });

  @override
  List<Object?> get props => [
        createdById,
        createdDate,
        title,
        content,
      ];
}
