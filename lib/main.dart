import 'package:blogapprentben/features/data/models/blog_model.dart';
import 'package:blogapprentben/features/presentation/screen/login/login_screen.dart';
import 'package:blogapprentben/features/presentation/screen/main/main_screen.dart';
import 'package:blogapprentben/util/app_color.dart';
import 'package:blogapprentben/util/app_key.dart';
import 'package:blogapprentben/util/route_generator.dart';
import 'package:blogapprentben/util/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:intl/date_symbol_data_local.dart';

import 'injection_container.dart' as di;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Hive.initFlutter();
  await initializeDateFormatting();

  Hive.registerAdapter(BlogModelAdapter());
  await Hive.openBox<BlogModel>(AppKey.hiveKeyBlogData);
  await Hive.openBox(AppKey.hiveBoxUserData);
  await di.init();

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  await Future.delayed(Duration(milliseconds: 300));
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    final box = Hive.box(AppKey.hiveBoxUserData);
    int? userId = box.get('userId');
    final home = userId != null ? MainScreen() : LoginScreen();

    return MaterialApp(
      title: 'Blog App Rentben',
      locale: const Locale('hu'),
      theme: ThemeData(
        primarySwatch: AppColor.createMaterialColor(AppColor.green),
      ),
      home: home,
      navigatorKey: AppKey.navigatorKey,
      onGenerateRoute: RouteGenerator.generateRoute,
    );
  }
}
