import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  const Failure([List properties = const <dynamic>[]]);

  @override
  List<Object> get props => [];
}

class UnexpectedFailure extends Failure {}

class LoginFailure extends Failure {}

class UserFailure extends Failure {}

class BlogFailure extends Failure {}
